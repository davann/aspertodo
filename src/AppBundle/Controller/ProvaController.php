<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class ProvaController extends Controller
{
    /**
     * @Route("/prova", name="prova")
     */
	
	public function numberAction() {
        $assignatura = $this->getDoctrine()
        ->getRepository('AppBundle:Assignatura')
        ->findAll();
        return $this->render('prova.html.twig', array('viewassignatura' => $assignatura));
     }

        /**
     * @Route("/proba", name="proba")
     */
    public function probaAction()
    {
        return $this->render('proba.html.twig');
    }
}
