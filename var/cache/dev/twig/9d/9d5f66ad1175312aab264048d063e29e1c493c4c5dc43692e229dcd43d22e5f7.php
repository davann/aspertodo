<?php

/* base.html.twig */
class __TwigTemplate_d81bae7d5325a7f23ba92e849144467ab8ed2421c201cbf1d70c17582ad76de7 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'body' => array($this, 'block_body'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_104068a41561d0f6608da275720caea655c6dc214045ecee1ffb7cc5e4db1e38 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_104068a41561d0f6608da275720caea655c6dc214045ecee1ffb7cc5e4db1e38->enter($__internal_104068a41561d0f6608da275720caea655c6dc214045ecee1ffb7cc5e4db1e38_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "base.html.twig"));

        $__internal_d012a6afc6a2a598f3ba923b0c62f297aba4755204b5b0ed040c2fc7e286d347 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_d012a6afc6a2a598f3ba923b0c62f297aba4755204b5b0ed040c2fc7e286d347->enter($__internal_d012a6afc6a2a598f3ba923b0c62f297aba4755204b5b0ed040c2fc7e286d347_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "base.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"UTF-8\" />
        <title>";
        // line 5
        $this->displayBlock('title', $context, $blocks);
        echo "</title>
        ";
        // line 6
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 7
        echo "        <link rel=\"icon\" type=\"image/x-icon\" href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("favicon.ico"), "html", null, true);
        echo "\" />
    </head>
    <body>
        ";
        // line 10
        $this->displayBlock('body', $context, $blocks);
        // line 11
        echo "        ";
        $this->displayBlock('javascripts', $context, $blocks);
        // line 12
        echo "    </body>
</html>
";
        
        $__internal_104068a41561d0f6608da275720caea655c6dc214045ecee1ffb7cc5e4db1e38->leave($__internal_104068a41561d0f6608da275720caea655c6dc214045ecee1ffb7cc5e4db1e38_prof);

        
        $__internal_d012a6afc6a2a598f3ba923b0c62f297aba4755204b5b0ed040c2fc7e286d347->leave($__internal_d012a6afc6a2a598f3ba923b0c62f297aba4755204b5b0ed040c2fc7e286d347_prof);

    }

    // line 5
    public function block_title($context, array $blocks = array())
    {
        $__internal_38f9ebb9c1d4b7cb3fd226c49c05297a0a3f4310f4959f0015f9aaebde510607 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_38f9ebb9c1d4b7cb3fd226c49c05297a0a3f4310f4959f0015f9aaebde510607->enter($__internal_38f9ebb9c1d4b7cb3fd226c49c05297a0a3f4310f4959f0015f9aaebde510607_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        $__internal_a2a4557b35cb31016441b6a7df39fcf03abd3636ed662b2935ccb7c9f6528787 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_a2a4557b35cb31016441b6a7df39fcf03abd3636ed662b2935ccb7c9f6528787->enter($__internal_a2a4557b35cb31016441b6a7df39fcf03abd3636ed662b2935ccb7c9f6528787_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Welcome!";
        
        $__internal_a2a4557b35cb31016441b6a7df39fcf03abd3636ed662b2935ccb7c9f6528787->leave($__internal_a2a4557b35cb31016441b6a7df39fcf03abd3636ed662b2935ccb7c9f6528787_prof);

        
        $__internal_38f9ebb9c1d4b7cb3fd226c49c05297a0a3f4310f4959f0015f9aaebde510607->leave($__internal_38f9ebb9c1d4b7cb3fd226c49c05297a0a3f4310f4959f0015f9aaebde510607_prof);

    }

    // line 6
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_4cbe6ea570a17a8943ac0374b4ab739e0866e343fa15ba4e860bee17932443d0 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_4cbe6ea570a17a8943ac0374b4ab739e0866e343fa15ba4e860bee17932443d0->enter($__internal_4cbe6ea570a17a8943ac0374b4ab739e0866e343fa15ba4e860bee17932443d0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        $__internal_c69ac3480ea78a9198190a6a7436d429d7baf5bf5ac86abab8d600d622537a62 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_c69ac3480ea78a9198190a6a7436d429d7baf5bf5ac86abab8d600d622537a62->enter($__internal_c69ac3480ea78a9198190a6a7436d429d7baf5bf5ac86abab8d600d622537a62_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        
        $__internal_c69ac3480ea78a9198190a6a7436d429d7baf5bf5ac86abab8d600d622537a62->leave($__internal_c69ac3480ea78a9198190a6a7436d429d7baf5bf5ac86abab8d600d622537a62_prof);

        
        $__internal_4cbe6ea570a17a8943ac0374b4ab739e0866e343fa15ba4e860bee17932443d0->leave($__internal_4cbe6ea570a17a8943ac0374b4ab739e0866e343fa15ba4e860bee17932443d0_prof);

    }

    // line 10
    public function block_body($context, array $blocks = array())
    {
        $__internal_f1a87d42fa1ac67e15b92d2fb6c0fc8ae7eeeefea80c579c64acd81974f4f96e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_f1a87d42fa1ac67e15b92d2fb6c0fc8ae7eeeefea80c579c64acd81974f4f96e->enter($__internal_f1a87d42fa1ac67e15b92d2fb6c0fc8ae7eeeefea80c579c64acd81974f4f96e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_675f8892f69824377ba7d16a303c5e4987564076305d28c766d724fde6cec8a0 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_675f8892f69824377ba7d16a303c5e4987564076305d28c766d724fde6cec8a0->enter($__internal_675f8892f69824377ba7d16a303c5e4987564076305d28c766d724fde6cec8a0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        
        $__internal_675f8892f69824377ba7d16a303c5e4987564076305d28c766d724fde6cec8a0->leave($__internal_675f8892f69824377ba7d16a303c5e4987564076305d28c766d724fde6cec8a0_prof);

        
        $__internal_f1a87d42fa1ac67e15b92d2fb6c0fc8ae7eeeefea80c579c64acd81974f4f96e->leave($__internal_f1a87d42fa1ac67e15b92d2fb6c0fc8ae7eeeefea80c579c64acd81974f4f96e_prof);

    }

    // line 11
    public function block_javascripts($context, array $blocks = array())
    {
        $__internal_5d775dfc494108ee1402c1ab323358b4b5616ec5b422f56161b9474c06c4d207 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_5d775dfc494108ee1402c1ab323358b4b5616ec5b422f56161b9474c06c4d207->enter($__internal_5d775dfc494108ee1402c1ab323358b4b5616ec5b422f56161b9474c06c4d207_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        $__internal_5b780af7cbc052e8bb179854a33ab989df505f43698ceb6df55ff24cc3b961d1 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_5b780af7cbc052e8bb179854a33ab989df505f43698ceb6df55ff24cc3b961d1->enter($__internal_5b780af7cbc052e8bb179854a33ab989df505f43698ceb6df55ff24cc3b961d1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        
        $__internal_5b780af7cbc052e8bb179854a33ab989df505f43698ceb6df55ff24cc3b961d1->leave($__internal_5b780af7cbc052e8bb179854a33ab989df505f43698ceb6df55ff24cc3b961d1_prof);

        
        $__internal_5d775dfc494108ee1402c1ab323358b4b5616ec5b422f56161b9474c06c4d207->leave($__internal_5d775dfc494108ee1402c1ab323358b4b5616ec5b422f56161b9474c06c4d207_prof);

    }

    public function getTemplateName()
    {
        return "base.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  117 => 11,  100 => 10,  83 => 6,  65 => 5,  53 => 12,  50 => 11,  48 => 10,  41 => 7,  39 => 6,  35 => 5,  29 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"UTF-8\" />
        <title>{% block title %}Welcome!{% endblock %}</title>
        {% block stylesheets %}{% endblock %}
        <link rel=\"icon\" type=\"image/x-icon\" href=\"{{ asset('favicon.ico') }}\" />
    </head>
    <body>
        {% block body %}{% endblock %}
        {% block javascripts %}{% endblock %}
    </body>
</html>
", "base.html.twig", "/home/ausias/Escriptori/Projectes/aspertodo/app/Resources/views/base.html.twig");
    }
}
