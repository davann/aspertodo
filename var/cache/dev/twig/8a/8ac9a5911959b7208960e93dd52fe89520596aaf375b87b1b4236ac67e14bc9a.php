<?php

/* @WebProfiler/Collector/exception.html.twig */
class __TwigTemplate_12f738d54aff3bf3f6a56dc81f2505fdcba5d762d3d028c6a50f1a44ad74da5e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/layout.html.twig", "@WebProfiler/Collector/exception.html.twig", 1);
        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'menu' => array($this, 'block_menu'),
            'panel' => array($this, 'block_panel'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_31b874cc9a473a62c4b6daee147bb92853cfbb7b63f8c65c5113f69348c00e5a = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_31b874cc9a473a62c4b6daee147bb92853cfbb7b63f8c65c5113f69348c00e5a->enter($__internal_31b874cc9a473a62c4b6daee147bb92853cfbb7b63f8c65c5113f69348c00e5a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/exception.html.twig"));

        $__internal_f6493a7b19f3ec26c6bc1b30a59de33fec339fb5bfe6934bf658a959a2553435 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_f6493a7b19f3ec26c6bc1b30a59de33fec339fb5bfe6934bf658a959a2553435->enter($__internal_f6493a7b19f3ec26c6bc1b30a59de33fec339fb5bfe6934bf658a959a2553435_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/exception.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_31b874cc9a473a62c4b6daee147bb92853cfbb7b63f8c65c5113f69348c00e5a->leave($__internal_31b874cc9a473a62c4b6daee147bb92853cfbb7b63f8c65c5113f69348c00e5a_prof);

        
        $__internal_f6493a7b19f3ec26c6bc1b30a59de33fec339fb5bfe6934bf658a959a2553435->leave($__internal_f6493a7b19f3ec26c6bc1b30a59de33fec339fb5bfe6934bf658a959a2553435_prof);

    }

    // line 3
    public function block_head($context, array $blocks = array())
    {
        $__internal_0ee33ed5fdb79b0e048fa56d8ceca5558ea0d8d199fe889585b7c1c39aa6803f = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_0ee33ed5fdb79b0e048fa56d8ceca5558ea0d8d199fe889585b7c1c39aa6803f->enter($__internal_0ee33ed5fdb79b0e048fa56d8ceca5558ea0d8d199fe889585b7c1c39aa6803f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        $__internal_b4162c3476a8b699cd91cf24df613014a8c296f82e29d597b5e501245ce34ed3 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_b4162c3476a8b699cd91cf24df613014a8c296f82e29d597b5e501245ce34ed3->enter($__internal_b4162c3476a8b699cd91cf24df613014a8c296f82e29d597b5e501245ce34ed3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        // line 4
        echo "    ";
        if ($this->getAttribute(($context["collector"] ?? $this->getContext($context, "collector")), "hasexception", array())) {
            // line 5
            echo "        <style>
            ";
            // line 6
            echo $this->env->getRuntime('Symfony\Bridge\Twig\Extension\HttpKernelRuntime')->renderFragment($this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_profiler_exception_css", array("token" => ($context["token"] ?? $this->getContext($context, "token")))));
            echo "
        </style>
    ";
        }
        // line 9
        echo "    ";
        $this->displayParentBlock("head", $context, $blocks);
        echo "
";
        
        $__internal_b4162c3476a8b699cd91cf24df613014a8c296f82e29d597b5e501245ce34ed3->leave($__internal_b4162c3476a8b699cd91cf24df613014a8c296f82e29d597b5e501245ce34ed3_prof);

        
        $__internal_0ee33ed5fdb79b0e048fa56d8ceca5558ea0d8d199fe889585b7c1c39aa6803f->leave($__internal_0ee33ed5fdb79b0e048fa56d8ceca5558ea0d8d199fe889585b7c1c39aa6803f_prof);

    }

    // line 12
    public function block_menu($context, array $blocks = array())
    {
        $__internal_e3df9d48917d1173a5eb9af8aad75df00742d47f9013c7ad4f497570e9f4579d = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_e3df9d48917d1173a5eb9af8aad75df00742d47f9013c7ad4f497570e9f4579d->enter($__internal_e3df9d48917d1173a5eb9af8aad75df00742d47f9013c7ad4f497570e9f4579d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        $__internal_1c48143a0e37973d25718d9a51c9aeddbcf9d16d831f0e542ba716551378d09b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_1c48143a0e37973d25718d9a51c9aeddbcf9d16d831f0e542ba716551378d09b->enter($__internal_1c48143a0e37973d25718d9a51c9aeddbcf9d16d831f0e542ba716551378d09b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        // line 13
        echo "    <span class=\"label ";
        echo (($this->getAttribute(($context["collector"] ?? $this->getContext($context, "collector")), "hasexception", array())) ? ("label-status-error") : ("disabled"));
        echo "\">
        <span class=\"icon\">";
        // line 14
        echo twig_include($this->env, $context, "@WebProfiler/Icon/exception.svg");
        echo "</span>
        <strong>Exception</strong>
        ";
        // line 16
        if ($this->getAttribute(($context["collector"] ?? $this->getContext($context, "collector")), "hasexception", array())) {
            // line 17
            echo "            <span class=\"count\">
                <span>1</span>
            </span>
        ";
        }
        // line 21
        echo "    </span>
";
        
        $__internal_1c48143a0e37973d25718d9a51c9aeddbcf9d16d831f0e542ba716551378d09b->leave($__internal_1c48143a0e37973d25718d9a51c9aeddbcf9d16d831f0e542ba716551378d09b_prof);

        
        $__internal_e3df9d48917d1173a5eb9af8aad75df00742d47f9013c7ad4f497570e9f4579d->leave($__internal_e3df9d48917d1173a5eb9af8aad75df00742d47f9013c7ad4f497570e9f4579d_prof);

    }

    // line 24
    public function block_panel($context, array $blocks = array())
    {
        $__internal_6671c72c2c299560b7a2366cb2f5c0a9721f7b952ae23e24c833f1b5c7a9d5ed = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_6671c72c2c299560b7a2366cb2f5c0a9721f7b952ae23e24c833f1b5c7a9d5ed->enter($__internal_6671c72c2c299560b7a2366cb2f5c0a9721f7b952ae23e24c833f1b5c7a9d5ed_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        $__internal_c76e877608c8d23d5317385b86e53677fe4a04f923d29e0872821fb4989aa4c3 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_c76e877608c8d23d5317385b86e53677fe4a04f923d29e0872821fb4989aa4c3->enter($__internal_c76e877608c8d23d5317385b86e53677fe4a04f923d29e0872821fb4989aa4c3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        // line 25
        echo "    <h2>Exceptions</h2>

    ";
        // line 27
        if ( !$this->getAttribute(($context["collector"] ?? $this->getContext($context, "collector")), "hasexception", array())) {
            // line 28
            echo "        <div class=\"empty\">
            <p>No exception was thrown and caught during the request.</p>
        </div>
    ";
        } else {
            // line 32
            echo "        <div class=\"sf-reset\">
            ";
            // line 33
            echo $this->env->getRuntime('Symfony\Bridge\Twig\Extension\HttpKernelRuntime')->renderFragment($this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_profiler_exception", array("token" => ($context["token"] ?? $this->getContext($context, "token")))));
            echo "
        </div>
    ";
        }
        
        $__internal_c76e877608c8d23d5317385b86e53677fe4a04f923d29e0872821fb4989aa4c3->leave($__internal_c76e877608c8d23d5317385b86e53677fe4a04f923d29e0872821fb4989aa4c3_prof);

        
        $__internal_6671c72c2c299560b7a2366cb2f5c0a9721f7b952ae23e24c833f1b5c7a9d5ed->leave($__internal_6671c72c2c299560b7a2366cb2f5c0a9721f7b952ae23e24c833f1b5c7a9d5ed_prof);

    }

    public function getTemplateName()
    {
        return "@WebProfiler/Collector/exception.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  138 => 33,  135 => 32,  129 => 28,  127 => 27,  123 => 25,  114 => 24,  103 => 21,  97 => 17,  95 => 16,  90 => 14,  85 => 13,  76 => 12,  63 => 9,  57 => 6,  54 => 5,  51 => 4,  42 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@WebProfiler/Profiler/layout.html.twig' %}

{% block head %}
    {% if collector.hasexception %}
        <style>
            {{ render(path('_profiler_exception_css', { token: token })) }}
        </style>
    {% endif %}
    {{ parent() }}
{% endblock %}

{% block menu %}
    <span class=\"label {{ collector.hasexception ? 'label-status-error' : 'disabled' }}\">
        <span class=\"icon\">{{ include('@WebProfiler/Icon/exception.svg') }}</span>
        <strong>Exception</strong>
        {% if collector.hasexception %}
            <span class=\"count\">
                <span>1</span>
            </span>
        {% endif %}
    </span>
{% endblock %}

{% block panel %}
    <h2>Exceptions</h2>

    {% if not collector.hasexception %}
        <div class=\"empty\">
            <p>No exception was thrown and caught during the request.</p>
        </div>
    {% else %}
        <div class=\"sf-reset\">
            {{ render(path('_profiler_exception', { token: token })) }}
        </div>
    {% endif %}
{% endblock %}
", "@WebProfiler/Collector/exception.html.twig", "/home/ausias/Escriptori/Projectes/aspertodo/vendor/symfony/symfony/src/Symfony/Bundle/WebProfilerBundle/Resources/views/Collector/exception.html.twig");
    }
}
