CREATE DATABASE aspertododb;

use aspertododb;

CREATE TABLE usuaris (
id_usuari int AUTO_INCREMENT,
nom_usuari VARCHAR(15) NOT NULL,
nom VARCHAR(15) NOT NULL,
cognom VARCHAR(20) NOT NULL,
contrassenya VARCHAR(10) NOT NULL,
imatge VARCHAR(100) NOT NULL DEFAULT 'img/user.png', 
id_rol int not null,
PRIMARY KEY (id_usuari),
foreign key (id_rol) references rol (id_rol)
)ENGINE=InnoDB;

CREATE TABLE rol (
	id_rol int AUTO_INCREMENT,
	r VARCHAR(10) NOT NULL,
PRIMARY KEY (id_rol)
)ENGINE=InnoDB;

CREATE TABLE tasca (
id_tasca int AUTO_INCREMENT,
nom_tasca varchar(15) not null,
hora_inici time not null,
hora_fi time not null,
id_assignatura int not null,
imatge_tasca VARCHAR(100) NOT NULL DEFAULT 'img/user.png',
descripcio varchar(50),
id_tasca_extra int not null,
id_usuari int not null,
PRIMARY KEY (id_tasca),
foreign key (id_usuari) references usuaris (id_usuari),
foreign key (id_assignatura) references assignatura (id_assignatura),
foreign key (id_tasca_extra) references tasca_extra (id_tasca_extra)
)ENGINE=InnoDB;

CREATE TABLE tasca_extra (
id_tasca_extra int AUTO_INCREMENT,
nom_tasca_extra varchar(15) not null,
descripcio varchar(50) not null,
imatge VARCHAR(100) NOT NULL DEFAULT 'img/user.png',
PRIMARY KEY (id_tasca_extra)
)ENGINE=InnoDB;

CREATE TABLE assignatura (
id_assignatura int AUTO_INCREMENT,
nom varchar(15) not null,
PRIMARY KEY (id_assignatura)
)ENGINE=InnoDB;
