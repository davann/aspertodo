<?php

/* @WebProfiler/Collector/router.html.twig */
class __TwigTemplate_490088ef8c2d536ba9aa3e4da532e3c9d5d6875dde7bb492995d7d77f4e311f2 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/layout.html.twig", "@WebProfiler/Collector/router.html.twig", 1);
        $this->blocks = array(
            'toolbar' => array($this, 'block_toolbar'),
            'menu' => array($this, 'block_menu'),
            'panel' => array($this, 'block_panel'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_377b636886fbfb172de1d8f961d14d2e4d99c9e058a9558b820253d24ef44fa2 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_377b636886fbfb172de1d8f961d14d2e4d99c9e058a9558b820253d24ef44fa2->enter($__internal_377b636886fbfb172de1d8f961d14d2e4d99c9e058a9558b820253d24ef44fa2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/router.html.twig"));

        $__internal_6e4ae15c57e6625d7c0fea440edec40ee19100dafefafe0d3827f33519cc3ce4 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_6e4ae15c57e6625d7c0fea440edec40ee19100dafefafe0d3827f33519cc3ce4->enter($__internal_6e4ae15c57e6625d7c0fea440edec40ee19100dafefafe0d3827f33519cc3ce4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/router.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_377b636886fbfb172de1d8f961d14d2e4d99c9e058a9558b820253d24ef44fa2->leave($__internal_377b636886fbfb172de1d8f961d14d2e4d99c9e058a9558b820253d24ef44fa2_prof);

        
        $__internal_6e4ae15c57e6625d7c0fea440edec40ee19100dafefafe0d3827f33519cc3ce4->leave($__internal_6e4ae15c57e6625d7c0fea440edec40ee19100dafefafe0d3827f33519cc3ce4_prof);

    }

    // line 3
    public function block_toolbar($context, array $blocks = array())
    {
        $__internal_78f1ad567b687a38a43ef152e169e2dceb1e67c7d0cbca71c08bde11f26ab5ab = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_78f1ad567b687a38a43ef152e169e2dceb1e67c7d0cbca71c08bde11f26ab5ab->enter($__internal_78f1ad567b687a38a43ef152e169e2dceb1e67c7d0cbca71c08bde11f26ab5ab_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "toolbar"));

        $__internal_640af61475623ec42f71a87f46d10defa5ab76707423747a64229ddae18bde4a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_640af61475623ec42f71a87f46d10defa5ab76707423747a64229ddae18bde4a->enter($__internal_640af61475623ec42f71a87f46d10defa5ab76707423747a64229ddae18bde4a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "toolbar"));

        
        $__internal_640af61475623ec42f71a87f46d10defa5ab76707423747a64229ddae18bde4a->leave($__internal_640af61475623ec42f71a87f46d10defa5ab76707423747a64229ddae18bde4a_prof);

        
        $__internal_78f1ad567b687a38a43ef152e169e2dceb1e67c7d0cbca71c08bde11f26ab5ab->leave($__internal_78f1ad567b687a38a43ef152e169e2dceb1e67c7d0cbca71c08bde11f26ab5ab_prof);

    }

    // line 5
    public function block_menu($context, array $blocks = array())
    {
        $__internal_d99357594bab5a4f2fd869b4e0732fa8967767c0fcb44c9bbfc3b92e608f9266 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_d99357594bab5a4f2fd869b4e0732fa8967767c0fcb44c9bbfc3b92e608f9266->enter($__internal_d99357594bab5a4f2fd869b4e0732fa8967767c0fcb44c9bbfc3b92e608f9266_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        $__internal_c339b12091f240242505b4d6e602880d56c8b4937bfd925542a33f616522ec8b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_c339b12091f240242505b4d6e602880d56c8b4937bfd925542a33f616522ec8b->enter($__internal_c339b12091f240242505b4d6e602880d56c8b4937bfd925542a33f616522ec8b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        // line 6
        echo "<span class=\"label\">
    <span class=\"icon\">";
        // line 7
        echo twig_include($this->env, $context, "@WebProfiler/Icon/router.svg");
        echo "</span>
    <strong>Routing</strong>
</span>
";
        
        $__internal_c339b12091f240242505b4d6e602880d56c8b4937bfd925542a33f616522ec8b->leave($__internal_c339b12091f240242505b4d6e602880d56c8b4937bfd925542a33f616522ec8b_prof);

        
        $__internal_d99357594bab5a4f2fd869b4e0732fa8967767c0fcb44c9bbfc3b92e608f9266->leave($__internal_d99357594bab5a4f2fd869b4e0732fa8967767c0fcb44c9bbfc3b92e608f9266_prof);

    }

    // line 12
    public function block_panel($context, array $blocks = array())
    {
        $__internal_6e15d7377d5c70130427383d938c0c6b169c9b7ff7b0f4e9936fa8345250bc7e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_6e15d7377d5c70130427383d938c0c6b169c9b7ff7b0f4e9936fa8345250bc7e->enter($__internal_6e15d7377d5c70130427383d938c0c6b169c9b7ff7b0f4e9936fa8345250bc7e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        $__internal_ae4c98763d21e144ee5a8a09da8bd0ed66ab6eca0084cfcec3e1e7057d89666d = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_ae4c98763d21e144ee5a8a09da8bd0ed66ab6eca0084cfcec3e1e7057d89666d->enter($__internal_ae4c98763d21e144ee5a8a09da8bd0ed66ab6eca0084cfcec3e1e7057d89666d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        // line 13
        echo "    ";
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Extension\HttpKernelRuntime')->renderFragment($this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_profiler_router", array("token" => ($context["token"] ?? $this->getContext($context, "token")))));
        echo "
";
        
        $__internal_ae4c98763d21e144ee5a8a09da8bd0ed66ab6eca0084cfcec3e1e7057d89666d->leave($__internal_ae4c98763d21e144ee5a8a09da8bd0ed66ab6eca0084cfcec3e1e7057d89666d_prof);

        
        $__internal_6e15d7377d5c70130427383d938c0c6b169c9b7ff7b0f4e9936fa8345250bc7e->leave($__internal_6e15d7377d5c70130427383d938c0c6b169c9b7ff7b0f4e9936fa8345250bc7e_prof);

    }

    public function getTemplateName()
    {
        return "@WebProfiler/Collector/router.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  94 => 13,  85 => 12,  71 => 7,  68 => 6,  59 => 5,  42 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@WebProfiler/Profiler/layout.html.twig' %}

{% block toolbar %}{% endblock %}

{% block menu %}
<span class=\"label\">
    <span class=\"icon\">{{ include('@WebProfiler/Icon/router.svg') }}</span>
    <strong>Routing</strong>
</span>
{% endblock %}

{% block panel %}
    {{ render(path('_profiler_router', { token: token })) }}
{% endblock %}
", "@WebProfiler/Collector/router.html.twig", "/home/ausias/Escriptori/Projectes/aspertodo/vendor/symfony/symfony/src/Symfony/Bundle/WebProfilerBundle/Resources/views/Collector/router.html.twig");
    }
}
