<?php

/* @FOSUser/Registration/register.html.twig */
class __TwigTemplate_3bbb6c6803d3f6a382be84877d6b3883a0ab65186a35fac3f7b35f8c4c47b064 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "@FOSUser/Registration/register.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_c65763249d2beeb35f73c30f79df99fc61627c22176afb7de183ed1d39cedced = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_c65763249d2beeb35f73c30f79df99fc61627c22176afb7de183ed1d39cedced->enter($__internal_c65763249d2beeb35f73c30f79df99fc61627c22176afb7de183ed1d39cedced_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/Registration/register.html.twig"));

        $__internal_971f2740c8e9daf79d181239f51a5e55280ea32b53d26b26044fc229c67aaee8 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_971f2740c8e9daf79d181239f51a5e55280ea32b53d26b26044fc229c67aaee8->enter($__internal_971f2740c8e9daf79d181239f51a5e55280ea32b53d26b26044fc229c67aaee8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/Registration/register.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_c65763249d2beeb35f73c30f79df99fc61627c22176afb7de183ed1d39cedced->leave($__internal_c65763249d2beeb35f73c30f79df99fc61627c22176afb7de183ed1d39cedced_prof);

        
        $__internal_971f2740c8e9daf79d181239f51a5e55280ea32b53d26b26044fc229c67aaee8->leave($__internal_971f2740c8e9daf79d181239f51a5e55280ea32b53d26b26044fc229c67aaee8_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_93a1b4c28ec86f0528a7a485a0b0ec3e37b8b3bd5bad384613708a0d86669fec = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_93a1b4c28ec86f0528a7a485a0b0ec3e37b8b3bd5bad384613708a0d86669fec->enter($__internal_93a1b4c28ec86f0528a7a485a0b0ec3e37b8b3bd5bad384613708a0d86669fec_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        $__internal_db1c4a4206b4925d111dbe1b980d2a418832ec1e42e5bd79ea6ed1b7fbf8f34d = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_db1c4a4206b4925d111dbe1b980d2a418832ec1e42e5bd79ea6ed1b7fbf8f34d->enter($__internal_db1c4a4206b4925d111dbe1b980d2a418832ec1e42e5bd79ea6ed1b7fbf8f34d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        $this->loadTemplate("@FOSUser/Registration/register_content.html.twig", "@FOSUser/Registration/register.html.twig", 4)->display($context);
        
        $__internal_db1c4a4206b4925d111dbe1b980d2a418832ec1e42e5bd79ea6ed1b7fbf8f34d->leave($__internal_db1c4a4206b4925d111dbe1b980d2a418832ec1e42e5bd79ea6ed1b7fbf8f34d_prof);

        
        $__internal_93a1b4c28ec86f0528a7a485a0b0ec3e37b8b3bd5bad384613708a0d86669fec->leave($__internal_93a1b4c28ec86f0528a7a485a0b0ec3e37b8b3bd5bad384613708a0d86669fec_prof);

    }

    public function getTemplateName()
    {
        return "@FOSUser/Registration/register.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"@FOSUser/layout.html.twig\" %}

{% block fos_user_content %}
{% include \"@FOSUser/Registration/register_content.html.twig\" %}
{% endblock fos_user_content %}
", "@FOSUser/Registration/register.html.twig", "/home/ausias/Escriptori/Projectes/aspertodo/vendor/friendsofsymfony/user-bundle/Resources/views/Registration/register.html.twig");
    }
}
