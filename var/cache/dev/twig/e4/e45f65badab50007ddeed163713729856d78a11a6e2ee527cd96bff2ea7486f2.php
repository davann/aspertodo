<?php

/* prova.html.twig */
class __TwigTemplate_07609b805e858f9943c8b30517eeef183592ee480636e0386495ec53e5e00634 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'body' => array($this, 'block_body'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_fef4e40a7932b29c799e3efd50c694de00ff4afb185b9f84d8d400da62228466 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_fef4e40a7932b29c799e3efd50c694de00ff4afb185b9f84d8d400da62228466->enter($__internal_fef4e40a7932b29c799e3efd50c694de00ff4afb185b9f84d8d400da62228466_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "prova.html.twig"));

        $__internal_30f626655c654401edd1628767568937ff963bf43c6f0f3df90887b1e7f692bf = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_30f626655c654401edd1628767568937ff963bf43c6f0f3df90887b1e7f692bf->enter($__internal_30f626655c654401edd1628767568937ff963bf43c6f0f3df90887b1e7f692bf_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "prova.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"UTF-8\" />
        <title>";
        // line 5
        $this->displayBlock('title', $context, $blocks);
        echo "</title>
        ";
        // line 6
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 7
        echo "        <link rel=\"icon\" type=\"image/x-icon\" href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("favicon.ico"), "html", null, true);
        echo "\" />
        <style>
        \t\ttable {
        \t\t\tborder: 2px black solid;
        \t\t}  
        \t\t
        \t\ttd {
        \t\t\tborder: 2px black solid;
        \t\t}    
        
        </style>
    </head>
    <body>
        ";
        // line 20
        $this->displayBlock('body', $context, $blocks);
        // line 32
        echo "        
        ";
        // line 33
        $this->displayBlock('javascripts', $context, $blocks);
        // line 34
        echo "    </body>
</html>
";
        
        $__internal_fef4e40a7932b29c799e3efd50c694de00ff4afb185b9f84d8d400da62228466->leave($__internal_fef4e40a7932b29c799e3efd50c694de00ff4afb185b9f84d8d400da62228466_prof);

        
        $__internal_30f626655c654401edd1628767568937ff963bf43c6f0f3df90887b1e7f692bf->leave($__internal_30f626655c654401edd1628767568937ff963bf43c6f0f3df90887b1e7f692bf_prof);

    }

    // line 5
    public function block_title($context, array $blocks = array())
    {
        $__internal_cfb917f99ccc2fbadf4c0c1eea505d0fd19e66ad67a38b9fd64a7b99f86a802b = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_cfb917f99ccc2fbadf4c0c1eea505d0fd19e66ad67a38b9fd64a7b99f86a802b->enter($__internal_cfb917f99ccc2fbadf4c0c1eea505d0fd19e66ad67a38b9fd64a7b99f86a802b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        $__internal_e9db7a83e73bcb4ff4cefc5663d8ec9d2c7b2e25e6ec42c45b41a32ed2c88830 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_e9db7a83e73bcb4ff4cefc5663d8ec9d2c7b2e25e6ec42c45b41a32ed2c88830->enter($__internal_e9db7a83e73bcb4ff4cefc5663d8ec9d2c7b2e25e6ec42c45b41a32ed2c88830_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Welcome!";
        
        $__internal_e9db7a83e73bcb4ff4cefc5663d8ec9d2c7b2e25e6ec42c45b41a32ed2c88830->leave($__internal_e9db7a83e73bcb4ff4cefc5663d8ec9d2c7b2e25e6ec42c45b41a32ed2c88830_prof);

        
        $__internal_cfb917f99ccc2fbadf4c0c1eea505d0fd19e66ad67a38b9fd64a7b99f86a802b->leave($__internal_cfb917f99ccc2fbadf4c0c1eea505d0fd19e66ad67a38b9fd64a7b99f86a802b_prof);

    }

    // line 6
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_9becc2628d8665b3c86760aa32758f3561c960ebb19b827bb6a94af9c0f00ed1 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_9becc2628d8665b3c86760aa32758f3561c960ebb19b827bb6a94af9c0f00ed1->enter($__internal_9becc2628d8665b3c86760aa32758f3561c960ebb19b827bb6a94af9c0f00ed1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        $__internal_1bd3b56dbe9312fd735c903db9f4fd1109e11d229c2863a4c0357b641c740fd0 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_1bd3b56dbe9312fd735c903db9f4fd1109e11d229c2863a4c0357b641c740fd0->enter($__internal_1bd3b56dbe9312fd735c903db9f4fd1109e11d229c2863a4c0357b641c740fd0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        
        $__internal_1bd3b56dbe9312fd735c903db9f4fd1109e11d229c2863a4c0357b641c740fd0->leave($__internal_1bd3b56dbe9312fd735c903db9f4fd1109e11d229c2863a4c0357b641c740fd0_prof);

        
        $__internal_9becc2628d8665b3c86760aa32758f3561c960ebb19b827bb6a94af9c0f00ed1->leave($__internal_9becc2628d8665b3c86760aa32758f3561c960ebb19b827bb6a94af9c0f00ed1_prof);

    }

    // line 20
    public function block_body($context, array $blocks = array())
    {
        $__internal_82d0d38fa2c1914786ce794dab558ede32350bb5b0907783890abdc9976ef029 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_82d0d38fa2c1914786ce794dab558ede32350bb5b0907783890abdc9976ef029->enter($__internal_82d0d38fa2c1914786ce794dab558ede32350bb5b0907783890abdc9976ef029_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_779f01a4499ab33fa44d0754b044536f82bb88b9f21e4f06d66a91b709349079 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_779f01a4499ab33fa44d0754b044536f82bb88b9f21e4f06d66a91b709349079->enter($__internal_779f01a4499ab33fa44d0754b044536f82bb88b9f21e4f06d66a91b709349079_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 21
        echo "        
        <table>
        <tr><td>ID Assignatura</td><td>Nom</td></tr>
\t\t\t";
        // line 24
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["viewassignatura"] ?? $this->getContext($context, "viewassignatura")));
        foreach ($context['_seq'] as $context["_key"] => $context["assignatura"]) {
            // line 25
            echo "\t\t\t
\t\t\t<tr>
\t\t\t<td>";
            // line 27
            echo twig_escape_filter($this->env, $this->getAttribute($context["assignatura"], "idAssignatura", array()), "html", null, true);
            echo "</td>
\t\t\t<td>";
            // line 28
            echo twig_escape_filter($this->env, $this->getAttribute($context["assignatura"], "nom", array()), "html", null, true);
            echo "</td>
\t\t\t</tr>
\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['assignatura'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 30
        echo "        
        ";
        
        $__internal_779f01a4499ab33fa44d0754b044536f82bb88b9f21e4f06d66a91b709349079->leave($__internal_779f01a4499ab33fa44d0754b044536f82bb88b9f21e4f06d66a91b709349079_prof);

        
        $__internal_82d0d38fa2c1914786ce794dab558ede32350bb5b0907783890abdc9976ef029->leave($__internal_82d0d38fa2c1914786ce794dab558ede32350bb5b0907783890abdc9976ef029_prof);

    }

    // line 33
    public function block_javascripts($context, array $blocks = array())
    {
        $__internal_4532705485320a50f92a46b153f214346b5dcf3c3d166e5b7806f0017d9ae3c9 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_4532705485320a50f92a46b153f214346b5dcf3c3d166e5b7806f0017d9ae3c9->enter($__internal_4532705485320a50f92a46b153f214346b5dcf3c3d166e5b7806f0017d9ae3c9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        $__internal_6e8156fc634ab19c9d56840ee9ab2e99f95b4e845c73eb8fb8922144e1f68a26 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_6e8156fc634ab19c9d56840ee9ab2e99f95b4e845c73eb8fb8922144e1f68a26->enter($__internal_6e8156fc634ab19c9d56840ee9ab2e99f95b4e845c73eb8fb8922144e1f68a26_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        
        $__internal_6e8156fc634ab19c9d56840ee9ab2e99f95b4e845c73eb8fb8922144e1f68a26->leave($__internal_6e8156fc634ab19c9d56840ee9ab2e99f95b4e845c73eb8fb8922144e1f68a26_prof);

        
        $__internal_4532705485320a50f92a46b153f214346b5dcf3c3d166e5b7806f0017d9ae3c9->leave($__internal_4532705485320a50f92a46b153f214346b5dcf3c3d166e5b7806f0017d9ae3c9_prof);

    }

    public function getTemplateName()
    {
        return "prova.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  158 => 33,  147 => 30,  138 => 28,  134 => 27,  130 => 25,  126 => 24,  121 => 21,  112 => 20,  95 => 6,  77 => 5,  65 => 34,  63 => 33,  60 => 32,  58 => 20,  41 => 7,  39 => 6,  35 => 5,  29 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"UTF-8\" />
        <title>{% block title %}Welcome!{% endblock %}</title>
        {% block stylesheets %}{% endblock %}
        <link rel=\"icon\" type=\"image/x-icon\" href=\"{{ asset('favicon.ico') }}\" />
        <style>
        \t\ttable {
        \t\t\tborder: 2px black solid;
        \t\t}  
        \t\t
        \t\ttd {
        \t\t\tborder: 2px black solid;
        \t\t}    
        
        </style>
    </head>
    <body>
        {% block body %}
        
        <table>
        <tr><td>ID Assignatura</td><td>Nom</td></tr>
\t\t\t{% for assignatura in viewassignatura %}
\t\t\t
\t\t\t<tr>
\t\t\t<td>{{assignatura.idAssignatura}}</td>
\t\t\t<td>{{assignatura.nom}}</td>
\t\t\t</tr>
\t\t\t{% endfor %}        
        {% endblock %}
        
        {% block javascripts %}{% endblock %}
    </body>
</html>
", "prova.html.twig", "/home/ausias/Escriptori/Projectes/aspertodo/app/Resources/views/prova.html.twig");
    }
}
