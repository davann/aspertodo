<?php

/* @FOSUser/Security/login.html.twig */
class __TwigTemplate_f902452bd4e2b5bd61051f47431cb025fc906d0603e67cac5f83a0eb7cfdef60 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "@FOSUser/Security/login.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_7c90185bef31bf546616f55564f14677ff2251e5cb11254372e29e986f7fd77d = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_7c90185bef31bf546616f55564f14677ff2251e5cb11254372e29e986f7fd77d->enter($__internal_7c90185bef31bf546616f55564f14677ff2251e5cb11254372e29e986f7fd77d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/Security/login.html.twig"));

        $__internal_efc469785c193ea7555a1c86ee60b7713ba6ba2518c03fcf163e0be4d47739b9 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_efc469785c193ea7555a1c86ee60b7713ba6ba2518c03fcf163e0be4d47739b9->enter($__internal_efc469785c193ea7555a1c86ee60b7713ba6ba2518c03fcf163e0be4d47739b9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/Security/login.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_7c90185bef31bf546616f55564f14677ff2251e5cb11254372e29e986f7fd77d->leave($__internal_7c90185bef31bf546616f55564f14677ff2251e5cb11254372e29e986f7fd77d_prof);

        
        $__internal_efc469785c193ea7555a1c86ee60b7713ba6ba2518c03fcf163e0be4d47739b9->leave($__internal_efc469785c193ea7555a1c86ee60b7713ba6ba2518c03fcf163e0be4d47739b9_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_02cd8b8713242ed9b8b78fa3759d326a7936a18eb374424c27c0e7491e9f39d1 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_02cd8b8713242ed9b8b78fa3759d326a7936a18eb374424c27c0e7491e9f39d1->enter($__internal_02cd8b8713242ed9b8b78fa3759d326a7936a18eb374424c27c0e7491e9f39d1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        $__internal_1c9b6fbc1cab9a600b80dfd69cf98ff500afd8320e754cc96f0b7778ff640240 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_1c9b6fbc1cab9a600b80dfd69cf98ff500afd8320e754cc96f0b7778ff640240->enter($__internal_1c9b6fbc1cab9a600b80dfd69cf98ff500afd8320e754cc96f0b7778ff640240_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        echo "    ";
        echo twig_include($this->env, $context, "@FOSUser/Security/login_content.html.twig");
        echo "
";
        
        $__internal_1c9b6fbc1cab9a600b80dfd69cf98ff500afd8320e754cc96f0b7778ff640240->leave($__internal_1c9b6fbc1cab9a600b80dfd69cf98ff500afd8320e754cc96f0b7778ff640240_prof);

        
        $__internal_02cd8b8713242ed9b8b78fa3759d326a7936a18eb374424c27c0e7491e9f39d1->leave($__internal_02cd8b8713242ed9b8b78fa3759d326a7936a18eb374424c27c0e7491e9f39d1_prof);

    }

    public function getTemplateName()
    {
        return "@FOSUser/Security/login.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"@FOSUser/layout.html.twig\" %}

{% block fos_user_content %}
    {{ include('@FOSUser/Security/login_content.html.twig') }}
{% endblock fos_user_content %}
", "@FOSUser/Security/login.html.twig", "/home/ausias/Escriptori/Projectes/aspertodo/vendor/friendsofsymfony/user-bundle/Resources/views/Security/login.html.twig");
    }
}
