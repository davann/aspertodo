<?php
// src/AppBundle/Entity/Tasca_extra.php
namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="tasca_extra")
 */
class Tasca_extra
{
    
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */  
    private $id_tasca_extra;
    
     /**
     * @ORM\Column(type="string", length=15)
     */
    private $nom_tasca_extra;
    
     /**
     * @ORM\Column(type="string", length=50)
     */
    private $descripcio;
    
    	/**
     * @ORM\Column(type="string", length=100)
     */
    private $imatge;

    /**
     * Get idTascaExtra
     *
     * @return integer
     */
    public function getIdTascaExtra()
    {
        return $this->id_tasca_extra;
    }

    /**
     * Set nomTascaExtra
     *
     * @param string $nomTascaExtra
     *
     * @return Tasca_extra
     */
    public function setNomTascaExtra($nomTascaExtra)
    {
        $this->nom_tasca_extra = $nomTascaExtra;

        return $this;
    }

    /**
     * Get nomTascaExtra
     *
     * @return string
     */
    public function getNomTascaExtra()
    {
        return $this->nom_tasca_extra;
    }

    /**
     * Set descripcio
     *
     * @param string $descripcio
     *
     * @return Tasca_extra
     */
    public function setDescripcio($descripcio)
    {
        $this->descripcio = $descripcio;

        return $this;
    }

    /**
     * Get descripcio
     *
     * @return string
     */
    public function getDescripcio()
    {
        return $this->descripcio;
    }

    /**
     * Set imatge
     *
     * @param string $imatge
     *
     * @return Tasca_extra
     */
    public function setImatge($imatge)
    {
        $this->imatge = $imatge;

        return $this;
    }

    /**
     * Get imatge
     *
     * @return string
     */
    public function getImatge()
    {
        return $this->imatge;
    }
}
