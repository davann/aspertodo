<?php

/* proba.html.twig */
class __TwigTemplate_67e93bfb1d463a2526ca7369d0422cc1d6166a16a1035605c717fa7d9a074877 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'body' => array($this, 'block_body'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_9da32e4f4115f8318975e494d4f99e3fa0dd87ec7e4725cc9f3b859fd21f3e0a = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_9da32e4f4115f8318975e494d4f99e3fa0dd87ec7e4725cc9f3b859fd21f3e0a->enter($__internal_9da32e4f4115f8318975e494d4f99e3fa0dd87ec7e4725cc9f3b859fd21f3e0a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "proba.html.twig"));

        $__internal_1cd5301c358eb6ac94776834e9d3d178bf676305a8cd769997eb35e0a5ee6497 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_1cd5301c358eb6ac94776834e9d3d178bf676305a8cd769997eb35e0a5ee6497->enter($__internal_1cd5301c358eb6ac94776834e9d3d178bf676305a8cd769997eb35e0a5ee6497_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "proba.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"UTF-8\" />
        <title>";
        // line 5
        $this->displayBlock('title', $context, $blocks);
        echo "</title>
        ";
        // line 6
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 7
        echo "        <link rel=\"icon\" type=\"image/x-icon\" href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("favicon.ico"), "html", null, true);
        echo "\" />
        
        <link rel=\"stylesheet\" href=\"";
        // line 9
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/style.css"), "html", null, true);
        echo "\" />
        <link rel=\"stylesheet\" href=\"";
        // line 10
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/bootstrap.min.css"), "html", null, true);
        echo "\" />
    </head>
    <body>
        ";
        // line 13
        $this->displayBlock('body', $context, $blocks);
        // line 14
        echo "        <h1 id=\"titol\">AsperToDo</h1>


        
        <div class=\"container-fluid\">
  <div class=\"row content\">
    <div class=\"col-sm-3 sidenav\">
      <ul class=\"nav nav-pills nav-stacked\">
        <li class=\"active\"><a href=\"#section1\">Calendari</a></li>
        <li><a href=\"#section2\">Gestió d'usuaris</a></li>
        <li><a href=\"#section3\">Tasques Extres</a></li>
      </ul><br>
      <div class=\"input-group\">
        <input type=\"text\" class=\"form-control\" placeholder=\"Search Blog..\">
        <span class=\"input-group-btn\">
          <button class=\"btn btn-default\" type=\"button\">
            <span class=\"glyphicon glyphicon-search\"></span>
          </button>
        </span>
      </div>
    </div>

    <div class=\"col-sm-9\">
      <h4><small>RECENT POSTS</small></h4>
      <hr>
      <h2>I Love Food</h2>
      <h5><span class=\"glyphicon glyphicon-time\"></span> Post by Jane Dane, Sep 27, 2015.</h5>
      <h5><span class=\"label label-danger\">Food</span> <span class=\"label label-primary\">Ipsum</span></h5><br>
      <p>Food is my passion. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
      <br><br>
      
      <h4><small>RECENT POSTS</small></h4>
      <hr>
      <h2>Officially Blogging</h2>
      <h5><span class=\"glyphicon glyphicon-time\"></span> Post by John Doe, Sep 24, 2015.</h5>
      <h5><span class=\"label label-success\">Lorem</span></h5><br>
      <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
      <hr>

      <h4>Leave a Comment:</h4>
      <form role=\"form\">
        <div class=\"form-group\">
          <textarea class=\"form-control\" rows=\"3\" required></textarea>
        </div>
        <button type=\"submit\" class=\"btn btn-success\">Submit</button>
      </form>
      <br><br>
      
      <p><span class=\"badge\">2</span> Comments:</p><br>
      
      <div class=\"row\">
        <div class=\"col-sm-2 text-center\">
          <img src=\"bandmember.jpg\" class=\"img-circle\" height=\"65\" width=\"65\" alt=\"Avatar\">
        </div>
        <div class=\"col-sm-10\">
          <h4>Anja <small>Sep 29, 2015, 9:12 PM</small></h4>
          <p>Keep up the GREAT work! I am cheering for you!! Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
          <br>
        </div>
        <div class=\"col-sm-2 text-center\">
          <img src=\"bird.jpg\" class=\"img-circle\" height=\"65\" width=\"65\" alt=\"Avatar\">
        </div>
        <div class=\"col-sm-10\">
          <h4>John Row <small>Sep 25, 2015, 8:25 PM</small></h4>
          <p>I am so happy for you man! Finally. I am looking forward to read about your trendy life. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
          <br>
          <p><span class=\"badge\">1</span> Comment:</p><br>
          <div class=\"row\">
            <div class=\"col-sm-2 text-center\">
              <img src=\"bird.jpg\" class=\"img-circle\" height=\"65\" width=\"65\" alt=\"Avatar\">
            </div>
            <div class=\"col-xs-10\">
              <h4>Nested Bro <small>Sep 25, 2015, 8:28 PM</small></h4>
              <p>Me too! WOW!</p>
              <br>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<footer class=\"container-fluid\">
  <p>Footer Text</p>
</footer>
        
        
        
        ";
        // line 103
        $this->displayBlock('javascripts', $context, $blocks);
        // line 107
        echo "    </body>
</html>
";
        
        $__internal_9da32e4f4115f8318975e494d4f99e3fa0dd87ec7e4725cc9f3b859fd21f3e0a->leave($__internal_9da32e4f4115f8318975e494d4f99e3fa0dd87ec7e4725cc9f3b859fd21f3e0a_prof);

        
        $__internal_1cd5301c358eb6ac94776834e9d3d178bf676305a8cd769997eb35e0a5ee6497->leave($__internal_1cd5301c358eb6ac94776834e9d3d178bf676305a8cd769997eb35e0a5ee6497_prof);

    }

    // line 5
    public function block_title($context, array $blocks = array())
    {
        $__internal_9aafc03c2fba785eb3012df473a2a794079787eeee0586b4a5561b8faae6eba9 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_9aafc03c2fba785eb3012df473a2a794079787eeee0586b4a5561b8faae6eba9->enter($__internal_9aafc03c2fba785eb3012df473a2a794079787eeee0586b4a5561b8faae6eba9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        $__internal_88c3fbb4dc28bd80b74ef9a6baba92156b4cc8201e76e7086b49dbd683ede91d = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_88c3fbb4dc28bd80b74ef9a6baba92156b4cc8201e76e7086b49dbd683ede91d->enter($__internal_88c3fbb4dc28bd80b74ef9a6baba92156b4cc8201e76e7086b49dbd683ede91d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Welcome!";
        
        $__internal_88c3fbb4dc28bd80b74ef9a6baba92156b4cc8201e76e7086b49dbd683ede91d->leave($__internal_88c3fbb4dc28bd80b74ef9a6baba92156b4cc8201e76e7086b49dbd683ede91d_prof);

        
        $__internal_9aafc03c2fba785eb3012df473a2a794079787eeee0586b4a5561b8faae6eba9->leave($__internal_9aafc03c2fba785eb3012df473a2a794079787eeee0586b4a5561b8faae6eba9_prof);

    }

    // line 6
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_a53373db43a01808308c0e5648cb5e48a6d8e9ad2f62e12ceb5922334f55eb90 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_a53373db43a01808308c0e5648cb5e48a6d8e9ad2f62e12ceb5922334f55eb90->enter($__internal_a53373db43a01808308c0e5648cb5e48a6d8e9ad2f62e12ceb5922334f55eb90_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        $__internal_07ec9d8da99810a95841847716564efcd47f8b4047603e95f7abbbc6884b0d08 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_07ec9d8da99810a95841847716564efcd47f8b4047603e95f7abbbc6884b0d08->enter($__internal_07ec9d8da99810a95841847716564efcd47f8b4047603e95f7abbbc6884b0d08_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        
        $__internal_07ec9d8da99810a95841847716564efcd47f8b4047603e95f7abbbc6884b0d08->leave($__internal_07ec9d8da99810a95841847716564efcd47f8b4047603e95f7abbbc6884b0d08_prof);

        
        $__internal_a53373db43a01808308c0e5648cb5e48a6d8e9ad2f62e12ceb5922334f55eb90->leave($__internal_a53373db43a01808308c0e5648cb5e48a6d8e9ad2f62e12ceb5922334f55eb90_prof);

    }

    // line 13
    public function block_body($context, array $blocks = array())
    {
        $__internal_8a49587116eb61eb8fe4349e70786fda1b26c64a9d62ebe6307336121a5c42cf = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_8a49587116eb61eb8fe4349e70786fda1b26c64a9d62ebe6307336121a5c42cf->enter($__internal_8a49587116eb61eb8fe4349e70786fda1b26c64a9d62ebe6307336121a5c42cf_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_112e57bc65acb5b13d6fdeea7d2a18d42f5867aa1642c9cb48eff4cf580766f7 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_112e57bc65acb5b13d6fdeea7d2a18d42f5867aa1642c9cb48eff4cf580766f7->enter($__internal_112e57bc65acb5b13d6fdeea7d2a18d42f5867aa1642c9cb48eff4cf580766f7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        
        $__internal_112e57bc65acb5b13d6fdeea7d2a18d42f5867aa1642c9cb48eff4cf580766f7->leave($__internal_112e57bc65acb5b13d6fdeea7d2a18d42f5867aa1642c9cb48eff4cf580766f7_prof);

        
        $__internal_8a49587116eb61eb8fe4349e70786fda1b26c64a9d62ebe6307336121a5c42cf->leave($__internal_8a49587116eb61eb8fe4349e70786fda1b26c64a9d62ebe6307336121a5c42cf_prof);

    }

    // line 103
    public function block_javascripts($context, array $blocks = array())
    {
        $__internal_deaaa7b5a8cbbf7923f20d8175865a0bf606d69226856c27f0a8d91021c86395 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_deaaa7b5a8cbbf7923f20d8175865a0bf606d69226856c27f0a8d91021c86395->enter($__internal_deaaa7b5a8cbbf7923f20d8175865a0bf606d69226856c27f0a8d91021c86395_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        $__internal_8b7c45356e53f5067fb4889cd7f42419c4b7317d430a4b5763f7c56182ab69cd = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_8b7c45356e53f5067fb4889cd7f42419c4b7317d430a4b5763f7c56182ab69cd->enter($__internal_8b7c45356e53f5067fb4889cd7f42419c4b7317d430a4b5763f7c56182ab69cd_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        // line 104
        echo "            <script src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/bootstrap.min.js"), "html", null, true);
        echo "\" />
            <script src=\"";
        // line 105
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/jquery-3.2.0.min.js"), "html", null, true);
        echo "\" />
        ";
        
        $__internal_8b7c45356e53f5067fb4889cd7f42419c4b7317d430a4b5763f7c56182ab69cd->leave($__internal_8b7c45356e53f5067fb4889cd7f42419c4b7317d430a4b5763f7c56182ab69cd_prof);

        
        $__internal_deaaa7b5a8cbbf7923f20d8175865a0bf606d69226856c27f0a8d91021c86395->leave($__internal_deaaa7b5a8cbbf7923f20d8175865a0bf606d69226856c27f0a8d91021c86395_prof);

    }

    public function getTemplateName()
    {
        return "proba.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  230 => 105,  225 => 104,  216 => 103,  199 => 13,  182 => 6,  164 => 5,  152 => 107,  150 => 103,  59 => 14,  57 => 13,  51 => 10,  47 => 9,  41 => 7,  39 => 6,  35 => 5,  29 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"UTF-8\" />
        <title>{% block title %}Welcome!{% endblock %}</title>
        {% block stylesheets %}{% endblock %}
        <link rel=\"icon\" type=\"image/x-icon\" href=\"{{ asset('favicon.ico') }}\" />
        
        <link rel=\"stylesheet\" href=\"{{ asset('css/style.css') }}\" />
        <link rel=\"stylesheet\" href=\"{{ asset('css/bootstrap.min.css') }}\" />
    </head>
    <body>
        {% block body %}{% endblock %}
        <h1 id=\"titol\">AsperToDo</h1>


        
        <div class=\"container-fluid\">
  <div class=\"row content\">
    <div class=\"col-sm-3 sidenav\">
      <ul class=\"nav nav-pills nav-stacked\">
        <li class=\"active\"><a href=\"#section1\">Calendari</a></li>
        <li><a href=\"#section2\">Gestió d'usuaris</a></li>
        <li><a href=\"#section3\">Tasques Extres</a></li>
      </ul><br>
      <div class=\"input-group\">
        <input type=\"text\" class=\"form-control\" placeholder=\"Search Blog..\">
        <span class=\"input-group-btn\">
          <button class=\"btn btn-default\" type=\"button\">
            <span class=\"glyphicon glyphicon-search\"></span>
          </button>
        </span>
      </div>
    </div>

    <div class=\"col-sm-9\">
      <h4><small>RECENT POSTS</small></h4>
      <hr>
      <h2>I Love Food</h2>
      <h5><span class=\"glyphicon glyphicon-time\"></span> Post by Jane Dane, Sep 27, 2015.</h5>
      <h5><span class=\"label label-danger\">Food</span> <span class=\"label label-primary\">Ipsum</span></h5><br>
      <p>Food is my passion. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
      <br><br>
      
      <h4><small>RECENT POSTS</small></h4>
      <hr>
      <h2>Officially Blogging</h2>
      <h5><span class=\"glyphicon glyphicon-time\"></span> Post by John Doe, Sep 24, 2015.</h5>
      <h5><span class=\"label label-success\">Lorem</span></h5><br>
      <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
      <hr>

      <h4>Leave a Comment:</h4>
      <form role=\"form\">
        <div class=\"form-group\">
          <textarea class=\"form-control\" rows=\"3\" required></textarea>
        </div>
        <button type=\"submit\" class=\"btn btn-success\">Submit</button>
      </form>
      <br><br>
      
      <p><span class=\"badge\">2</span> Comments:</p><br>
      
      <div class=\"row\">
        <div class=\"col-sm-2 text-center\">
          <img src=\"bandmember.jpg\" class=\"img-circle\" height=\"65\" width=\"65\" alt=\"Avatar\">
        </div>
        <div class=\"col-sm-10\">
          <h4>Anja <small>Sep 29, 2015, 9:12 PM</small></h4>
          <p>Keep up the GREAT work! I am cheering for you!! Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
          <br>
        </div>
        <div class=\"col-sm-2 text-center\">
          <img src=\"bird.jpg\" class=\"img-circle\" height=\"65\" width=\"65\" alt=\"Avatar\">
        </div>
        <div class=\"col-sm-10\">
          <h4>John Row <small>Sep 25, 2015, 8:25 PM</small></h4>
          <p>I am so happy for you man! Finally. I am looking forward to read about your trendy life. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
          <br>
          <p><span class=\"badge\">1</span> Comment:</p><br>
          <div class=\"row\">
            <div class=\"col-sm-2 text-center\">
              <img src=\"bird.jpg\" class=\"img-circle\" height=\"65\" width=\"65\" alt=\"Avatar\">
            </div>
            <div class=\"col-xs-10\">
              <h4>Nested Bro <small>Sep 25, 2015, 8:28 PM</small></h4>
              <p>Me too! WOW!</p>
              <br>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<footer class=\"container-fluid\">
  <p>Footer Text</p>
</footer>
        
        
        
        {% block javascripts %}
            <script src=\"{{ asset('js/bootstrap.min.js') }}\" />
            <script src=\"{{ asset('js/jquery-3.2.0.min.js') }}\" />
        {% endblock %}
    </body>
</html>
", "proba.html.twig", "/home/ausias/Escriptori/nou/aspertodo/app/Resources/views/proba.html.twig");
    }
}
