<?php

/* @Twig/Exception/exception_full.html.twig */
class __TwigTemplate_f5a4e4e47a61dbc596b8a5cf6c4251cf9809cbaa7fe0192c935bf3aa4bd7f2e4 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@Twig/layout.html.twig", "@Twig/Exception/exception_full.html.twig", 1);
        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@Twig/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_99fb7164c3413cbeaba2202af5b0f75ef74f94af89b2f2a653601ae0bcabda49 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_99fb7164c3413cbeaba2202af5b0f75ef74f94af89b2f2a653601ae0bcabda49->enter($__internal_99fb7164c3413cbeaba2202af5b0f75ef74f94af89b2f2a653601ae0bcabda49_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/exception_full.html.twig"));

        $__internal_4c65df73bfe1e4963541a82d3cb01e82df97b63663f78a722af86b8f9759c5d3 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_4c65df73bfe1e4963541a82d3cb01e82df97b63663f78a722af86b8f9759c5d3->enter($__internal_4c65df73bfe1e4963541a82d3cb01e82df97b63663f78a722af86b8f9759c5d3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/exception_full.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_99fb7164c3413cbeaba2202af5b0f75ef74f94af89b2f2a653601ae0bcabda49->leave($__internal_99fb7164c3413cbeaba2202af5b0f75ef74f94af89b2f2a653601ae0bcabda49_prof);

        
        $__internal_4c65df73bfe1e4963541a82d3cb01e82df97b63663f78a722af86b8f9759c5d3->leave($__internal_4c65df73bfe1e4963541a82d3cb01e82df97b63663f78a722af86b8f9759c5d3_prof);

    }

    // line 3
    public function block_head($context, array $blocks = array())
    {
        $__internal_8f6728004290952b37dc0094a46c1444ca0ceb32a24d03684568c57f5739021c = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_8f6728004290952b37dc0094a46c1444ca0ceb32a24d03684568c57f5739021c->enter($__internal_8f6728004290952b37dc0094a46c1444ca0ceb32a24d03684568c57f5739021c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        $__internal_38d42e8cb608aa3f8cb5fa943fdbb7dad9799425cc1b7aee3e3b5a6a3d86639b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_38d42e8cb608aa3f8cb5fa943fdbb7dad9799425cc1b7aee3e3b5a6a3d86639b->enter($__internal_38d42e8cb608aa3f8cb5fa943fdbb7dad9799425cc1b7aee3e3b5a6a3d86639b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        // line 4
        echo "    <style>
        .sf-reset .traces {
            padding-bottom: 14px;
        }
        .sf-reset .traces li {
            font-size: 12px;
            color: #868686;
            padding: 5px 4px;
            list-style-type: decimal;
            margin-left: 20px;
        }
        .sf-reset #logs .traces li.error {
            font-style: normal;
            color: #AA3333;
            background: #f9ecec;
        }
        .sf-reset #logs .traces li.warning {
            font-style: normal;
            background: #ffcc00;
        }
        /* fix for Opera not liking empty <li> */
        .sf-reset .traces li:after {
            content: \"\\00A0\";
        }
        .sf-reset .trace {
            border: 1px solid #D3D3D3;
            padding: 10px;
            overflow: auto;
            margin: 10px 0 20px;
        }
        .sf-reset .block-exception {
            -moz-border-radius: 16px;
            -webkit-border-radius: 16px;
            border-radius: 16px;
            margin-bottom: 20px;
            background-color: #f6f6f6;
            border: 1px solid #dfdfdf;
            padding: 30px 28px;
            word-wrap: break-word;
            overflow: hidden;
        }
        .sf-reset .block-exception div {
            color: #313131;
            font-size: 10px;
        }
        .sf-reset .block-exception-detected .illustration-exception,
        .sf-reset .block-exception-detected .text-exception {
            float: left;
        }
        .sf-reset .block-exception-detected .illustration-exception {
            width: 152px;
        }
        .sf-reset .block-exception-detected .text-exception {
            width: 670px;
            padding: 30px 44px 24px 46px;
            position: relative;
        }
        .sf-reset .text-exception .open-quote,
        .sf-reset .text-exception .close-quote {
            font-family: Arial, Helvetica, sans-serif;
            position: absolute;
            color: #C9C9C9;
            font-size: 8em;
        }
        .sf-reset .open-quote {
            top: 0;
            left: 0;
        }
        .sf-reset .close-quote {
            bottom: -0.5em;
            right: 50px;
        }
        .sf-reset .block-exception p {
            font-family: Arial, Helvetica, sans-serif;
        }
        .sf-reset .block-exception p a,
        .sf-reset .block-exception p a:hover {
            color: #565656;
        }
        .sf-reset .logs h2 {
            float: left;
            width: 654px;
        }
        .sf-reset .error-count, .sf-reset .support {
            float: right;
            width: 170px;
            text-align: right;
        }
        .sf-reset .error-count span {
             display: inline-block;
             background-color: #aacd4e;
             -moz-border-radius: 6px;
             -webkit-border-radius: 6px;
             border-radius: 6px;
             padding: 4px;
             color: white;
             margin-right: 2px;
             font-size: 11px;
             font-weight: bold;
        }

        .sf-reset .support a {
            display: inline-block;
            -moz-border-radius: 6px;
            -webkit-border-radius: 6px;
            border-radius: 6px;
            padding: 4px;
            color: #000000;
            margin-right: 2px;
            font-size: 11px;
            font-weight: bold;
        }

        .sf-reset .toggle {
            vertical-align: middle;
        }
        .sf-reset .linked ul,
        .sf-reset .linked li {
            display: inline;
        }
        .sf-reset #output-content {
            color: #000;
            font-size: 12px;
        }
        .sf-reset #traces-text pre {
            white-space: pre;
            font-size: 12px;
            font-family: monospace;
        }
    </style>
";
        
        $__internal_38d42e8cb608aa3f8cb5fa943fdbb7dad9799425cc1b7aee3e3b5a6a3d86639b->leave($__internal_38d42e8cb608aa3f8cb5fa943fdbb7dad9799425cc1b7aee3e3b5a6a3d86639b_prof);

        
        $__internal_8f6728004290952b37dc0094a46c1444ca0ceb32a24d03684568c57f5739021c->leave($__internal_8f6728004290952b37dc0094a46c1444ca0ceb32a24d03684568c57f5739021c_prof);

    }

    // line 136
    public function block_title($context, array $blocks = array())
    {
        $__internal_60c5a21a78c20860e69632d9cfcabefe17c7b84b3f41c8cd582124cfb248eac4 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_60c5a21a78c20860e69632d9cfcabefe17c7b84b3f41c8cd582124cfb248eac4->enter($__internal_60c5a21a78c20860e69632d9cfcabefe17c7b84b3f41c8cd582124cfb248eac4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        $__internal_d1780e8abce1b4a06f4916ad54a73a649fa87bdfde7af67619e63d055be60edd = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_d1780e8abce1b4a06f4916ad54a73a649fa87bdfde7af67619e63d055be60edd->enter($__internal_d1780e8abce1b4a06f4916ad54a73a649fa87bdfde7af67619e63d055be60edd_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        // line 137
        echo "    ";
        echo twig_escape_filter($this->env, $this->getAttribute(($context["exception"] ?? $this->getContext($context, "exception")), "message", array()), "html", null, true);
        echo " (";
        echo twig_escape_filter($this->env, ($context["status_code"] ?? $this->getContext($context, "status_code")), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, ($context["status_text"] ?? $this->getContext($context, "status_text")), "html", null, true);
        echo ")
";
        
        $__internal_d1780e8abce1b4a06f4916ad54a73a649fa87bdfde7af67619e63d055be60edd->leave($__internal_d1780e8abce1b4a06f4916ad54a73a649fa87bdfde7af67619e63d055be60edd_prof);

        
        $__internal_60c5a21a78c20860e69632d9cfcabefe17c7b84b3f41c8cd582124cfb248eac4->leave($__internal_60c5a21a78c20860e69632d9cfcabefe17c7b84b3f41c8cd582124cfb248eac4_prof);

    }

    // line 140
    public function block_body($context, array $blocks = array())
    {
        $__internal_4aa6c19ab3cc22a9a13969b42acb373206e04482783cc6baab7c9a11b1bb4433 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_4aa6c19ab3cc22a9a13969b42acb373206e04482783cc6baab7c9a11b1bb4433->enter($__internal_4aa6c19ab3cc22a9a13969b42acb373206e04482783cc6baab7c9a11b1bb4433_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_c4ef3c7deece573b0cdb56a31e1559b0092cc7112d8134106a2c6f1c75ad1171 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_c4ef3c7deece573b0cdb56a31e1559b0092cc7112d8134106a2c6f1c75ad1171->enter($__internal_c4ef3c7deece573b0cdb56a31e1559b0092cc7112d8134106a2c6f1c75ad1171_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 141
        echo "    ";
        $this->loadTemplate("@Twig/Exception/exception.html.twig", "@Twig/Exception/exception_full.html.twig", 141)->display($context);
        
        $__internal_c4ef3c7deece573b0cdb56a31e1559b0092cc7112d8134106a2c6f1c75ad1171->leave($__internal_c4ef3c7deece573b0cdb56a31e1559b0092cc7112d8134106a2c6f1c75ad1171_prof);

        
        $__internal_4aa6c19ab3cc22a9a13969b42acb373206e04482783cc6baab7c9a11b1bb4433->leave($__internal_4aa6c19ab3cc22a9a13969b42acb373206e04482783cc6baab7c9a11b1bb4433_prof);

    }

    public function getTemplateName()
    {
        return "@Twig/Exception/exception_full.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  226 => 141,  217 => 140,  200 => 137,  191 => 136,  51 => 4,  42 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@Twig/layout.html.twig' %}

{% block head %}
    <style>
        .sf-reset .traces {
            padding-bottom: 14px;
        }
        .sf-reset .traces li {
            font-size: 12px;
            color: #868686;
            padding: 5px 4px;
            list-style-type: decimal;
            margin-left: 20px;
        }
        .sf-reset #logs .traces li.error {
            font-style: normal;
            color: #AA3333;
            background: #f9ecec;
        }
        .sf-reset #logs .traces li.warning {
            font-style: normal;
            background: #ffcc00;
        }
        /* fix for Opera not liking empty <li> */
        .sf-reset .traces li:after {
            content: \"\\00A0\";
        }
        .sf-reset .trace {
            border: 1px solid #D3D3D3;
            padding: 10px;
            overflow: auto;
            margin: 10px 0 20px;
        }
        .sf-reset .block-exception {
            -moz-border-radius: 16px;
            -webkit-border-radius: 16px;
            border-radius: 16px;
            margin-bottom: 20px;
            background-color: #f6f6f6;
            border: 1px solid #dfdfdf;
            padding: 30px 28px;
            word-wrap: break-word;
            overflow: hidden;
        }
        .sf-reset .block-exception div {
            color: #313131;
            font-size: 10px;
        }
        .sf-reset .block-exception-detected .illustration-exception,
        .sf-reset .block-exception-detected .text-exception {
            float: left;
        }
        .sf-reset .block-exception-detected .illustration-exception {
            width: 152px;
        }
        .sf-reset .block-exception-detected .text-exception {
            width: 670px;
            padding: 30px 44px 24px 46px;
            position: relative;
        }
        .sf-reset .text-exception .open-quote,
        .sf-reset .text-exception .close-quote {
            font-family: Arial, Helvetica, sans-serif;
            position: absolute;
            color: #C9C9C9;
            font-size: 8em;
        }
        .sf-reset .open-quote {
            top: 0;
            left: 0;
        }
        .sf-reset .close-quote {
            bottom: -0.5em;
            right: 50px;
        }
        .sf-reset .block-exception p {
            font-family: Arial, Helvetica, sans-serif;
        }
        .sf-reset .block-exception p a,
        .sf-reset .block-exception p a:hover {
            color: #565656;
        }
        .sf-reset .logs h2 {
            float: left;
            width: 654px;
        }
        .sf-reset .error-count, .sf-reset .support {
            float: right;
            width: 170px;
            text-align: right;
        }
        .sf-reset .error-count span {
             display: inline-block;
             background-color: #aacd4e;
             -moz-border-radius: 6px;
             -webkit-border-radius: 6px;
             border-radius: 6px;
             padding: 4px;
             color: white;
             margin-right: 2px;
             font-size: 11px;
             font-weight: bold;
        }

        .sf-reset .support a {
            display: inline-block;
            -moz-border-radius: 6px;
            -webkit-border-radius: 6px;
            border-radius: 6px;
            padding: 4px;
            color: #000000;
            margin-right: 2px;
            font-size: 11px;
            font-weight: bold;
        }

        .sf-reset .toggle {
            vertical-align: middle;
        }
        .sf-reset .linked ul,
        .sf-reset .linked li {
            display: inline;
        }
        .sf-reset #output-content {
            color: #000;
            font-size: 12px;
        }
        .sf-reset #traces-text pre {
            white-space: pre;
            font-size: 12px;
            font-family: monospace;
        }
    </style>
{% endblock %}

{% block title %}
    {{ exception.message }} ({{ status_code }} {{ status_text }})
{% endblock %}

{% block body %}
    {% include '@Twig/Exception/exception.html.twig' %}
{% endblock %}
", "@Twig/Exception/exception_full.html.twig", "/home/ausias/Escriptori/nou/aspertodo/vendor/symfony/symfony/src/Symfony/Bundle/TwigBundle/Resources/views/Exception/exception_full.html.twig");
    }
}
