<?php
// src/AppBundle/Entity/Assignatura.php
namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="assignatura")
 */

class Assignatura
{
	  /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */    
    private $id_assignatura;
    
    /**
     * @ORM\Column(type="string", length=15)
     */
    private $nom;

    /**
     * Get idAssignatura
     *
     * @return integer
     */
    public function getIdAssignatura()
    {
        return $this->id_assignatura;
    }

    /**
     * Set nom
     *
     * @param string $nom
     *
     * @return Assignatura
     */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get nom
     *
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }
}
